/* compute optimal solutions for sliding block puzzle. */
#include <SDL2/SDL.h>
#include <stdio.h>
#include <cstdlib>   /* for atexit() */
#include <algorithm>
using std::swap;
#include <cassert>
#include <vector>
#include <list>
#include <deque>
using namespace std;

/* SDL reference: https://wiki.libsdl.org/CategoryAPI */

/* initial size; will be set to screen size after window creation. */
int SCREEN_WIDTH = 640;
int SCREEN_HEIGHT = 480;
int fcount = 0;
int mousestate = 0;
SDL_Point lastm = {0,0}; /* last mouse coords */
SDL_Rect bframe; /* bounding rectangle of board */
static const int ep = 2; /* epsilon offset from grid lines */

bool init(); /* setup SDL */
void initBlocks();

//#define FULLSCREEN_FLAG SDL_WINDOW_FULLSCREEN_DESKTOP
#define FULLSCREEN_FLAG 0

/* NOTE: ssq == "small square", lsq == "large square" */
enum bType {hor,ver,ssq,lsq};
struct block {
	SDL_Rect R; /* screen coords + dimensions */
	bType type; /* shape + orientation */
	/* TODO: you might want to add other useful information to
	 * this struct, like where it is attached on the board.
	 * (Alternatively, you could just compute this from R.x and R.y,
	 * but it might be convenient to store it directly.) */
	void rotate() /* rotate rectangular pieces */
	{
		if (type != hor && type != ver) return;
		type = (type==hor)?ver:hor;
		swap(R.w,R.h);
	}
	int column;
    int row;
};

#define NBLOCKS 10
block B[NBLOCKS];
block* dragged = NULL;

block* findBlock(int x, int y);
void close(); /* call this at end of main loop to free SDL resources */
SDL_Window* gWindow = 0; /* main window */
SDL_Renderer* gRenderer = 0;

/*
Board notation
+----------+--------------+
| Notation | Meaning      |
+----------+--------------+
|    0     |     empty    |
|   1-5    |     rect     |
|   6-9    | small square |
|   10     | large square |
+----------+--------------+
Board coordinates
 - (0,0) top left corner
 - (3,4) bottom right corner
*/
// 2D array
const int board_width = 4;
const int board_height = 5; 

/*
Formula for mapping pixel coordinates to board coordinates
- bframe is the board's top left most corner
- cell is length of board cell, which is defined in initBlocks as
  (SCREEN_HEIGHT * 3)/(4 * 5) - (2*ep)
- ep is the space need to draw grid lines, which is set to 2 at the top
  of this file
+--------------------------+---------------+
|       pixel x coord      | board x coord |
+--------------------------+---------------+
| bframe.x + ep            |       0       |
| bframe.x +   cell + 2*ep |       1       |
| bframe.x + 2*cell + 3*ep |       2       |
| bframe.x + 3*cell + 4*ep |       3       |
+--------------------------+---------------+
*/
int board_r[board_width][board_height];

void computeBoardRepresentation()
{
	int i, j;
	int rep_x, rep_y;
	int cell = (SCREEN_HEIGHT * 3)/(4 * 5) - (2*ep);
	int rec_base = 1;
	int ssq_base = 6;
	int lsq_base = 9;
	int rec_count = 0, sqr_count = 0;
	// Assign board initial empty values
	for (i = 0; i < board_height; i++) {
		for (j = 0; j < board_width; j++) {
			board_r[i][j] = 0;
		}
	}
	for (i = 0; i < NBLOCKS; i++) {
		rep_x = (B[i].R.x - bframe.x) / cell;
		rep_y = (B[i].R.y - bframe.y) / cell;
		// Place block on board
		switch (B[i].type) {
			case hor:
				// Since the current block is horizontal, it spans across two columns
				board_r[rep_y][  rep_x  ] = rec_base ;
				board_r[rep_y][rep_x + 1] = rec_base + 1;
				rec_count++;
				break;
			case ver:
				// Since the current block is vertical, it spans across two rows
				board_r[  rep_y  ][rep_x] = rec_base + 2;
				board_r[rep_y + 1][rep_x] = rec_base + 3;
				rec_count++;
				break;
			case ssq:
				board_r[rep_y][rep_x] = ssq_base; //+ sqr_count;
				sqr_count++;
				break;
			case lsq:
				board_r[  rep_y  ][  rep_x  ] = lsq_base;
				board_r[  rep_y  ][rep_x + 1] = lsq_base;
				board_r[rep_y + 1][  rep_x  ] = lsq_base;
				board_r[rep_y + 1][rep_x + 1] = lsq_base;
				break;
		}
	}
}

void printBoardRepresentation(){
	int i, j;
	printf("Board representation\n\n");
	for (i = 0; i <  board_height; i++) {
		for (j = 0; j < board_width; j++) {
			// Print board value with spacing of 3
			printf("%3d", board_r[i][j]);
		}
		printf("\n");
	}
}

bool init()
{
	if(SDL_Init(SDL_INIT_VIDEO) < 0) {
		printf("SDL_Init failed.  Error: %s\n", SDL_GetError());
		return false;
	}
	/* NOTE: take this out if you have issues, say in a virtualized
	 * environment: */
	if(!SDL_SetHint(SDL_HINT_RENDER_VSYNC, "1")) {
		printf("Warning: vsync hint didn't work.\n");
	}
	/* create main window */
	gWindow = SDL_CreateWindow("Sliding block puzzle solver",
								SDL_WINDOWPOS_UNDEFINED,
								SDL_WINDOWPOS_UNDEFINED,
								SCREEN_WIDTH, SCREEN_HEIGHT,
								SDL_WINDOW_SHOWN|FULLSCREEN_FLAG);
	if(!gWindow) {
		printf("Failed to create main window. SDL Error: %s\n", SDL_GetError());
		return false;
	}
	/* set width and height */
	SDL_GetWindowSize(gWindow, &SCREEN_WIDTH, &SCREEN_HEIGHT);
	/* setup renderer with frame-sync'd drawing: */
	gRenderer = SDL_CreateRenderer(gWindow, -1,
			SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if(!gRenderer) {
		printf("Failed to create renderer. SDL Error: %s\n", SDL_GetError());
		return false;
	}
	SDL_SetRenderDrawBlendMode(gRenderer,SDL_BLENDMODE_BLEND);

	initBlocks();
	return true;
}

/* TODO: you'll probably want a function that takes a state / configuration
 * and arranges the blocks in accord.  This will be useful for stepping
 * through a solution.  Be careful to ensure your underlying representation
 * stays in sync with what's drawn on the screen... */

void initBlocks()
{
	int& W = SCREEN_WIDTH;
	int& H = SCREEN_HEIGHT;
	int h = H*3/4;
	int w = 4*h/5;
	int u = h/5-2*ep;
	int mw = (W-w)/2;
	int mh = (H-h)/2;

	/* setup bounding rectangle of the board: */
	bframe.x = (W-w)/2;
	bframe.y = (H-h)/2;
	bframe.w = w;
	bframe.h = h;

	/* NOTE: there is a tacit assumption that should probably be
	 * made explicit: blocks 0--4 are the rectangles, 5-8 are small
	 * squares, and 9 is the big square.  This is assumed by the
	 * drawBlocks function below. */

	for (size_t i = 0; i < 5; i++) {
		B[i].R.x = (mw-2*u)/2;
		B[i].R.y = mh + (i+1)*(u/5) + i*u;
		B[i].R.w = 2*(u+ep);
		B[i].R.h = u;
		B[i].type = hor;
	}
	B[4].R.x = mw+ep;
	B[4].R.y = mh+ep;
	B[4].R.w = 2*(u+ep);
	B[4].R.h = u;
	B[4].type = hor;
	/* small squares */
	for (size_t i = 0; i < 4; i++) {
		B[i+5].R.x = (W+w)/2 + (mw-2*u)/2 + (i%2)*(u+u/5);
		B[i+5].R.y = mh + ((i/2)+1)*(u/5) + (i/2)*u;
		B[i+5].R.w = u;
		B[i+5].R.h = u;
		B[i+5].type = ssq;
	}
	B[9].R.x = B[5].R.x + u/10;
	B[9].R.y = B[7].R.y + u + 2*u/5;
	B[9].R.w = 2*(u+ep);
	B[9].R.h = 2*(u+ep);
	B[9].type = lsq;
}

void drawBlocks()
{
	/* rectangles */
	SDL_SetRenderDrawColor(gRenderer, 0x43, 0x4c, 0x5e, 0xff);
	for (size_t i = 0; i < 5; i++) {
		SDL_RenderFillRect(gRenderer,&B[i].R);
	}
	/* small squares */
	SDL_SetRenderDrawColor(gRenderer, 0x5e, 0x81, 0xac, 0xff);
	for (size_t i = 5; i < 9; i++) {
		SDL_RenderFillRect(gRenderer,&B[i].R);
	}
	/* large square */
	SDL_SetRenderDrawColor(gRenderer, 0xa3, 0xbe, 0x8c, 0xff);
	SDL_RenderFillRect(gRenderer,&B[9].R);
}

void setPuzzle()
{
	int uw = bframe.w/4;
	int uh = bframe.h/5;

		for (size_t i = 0; i < 4; i++)
		{
			B[i].rotate();
		}

		B[0].R.x = bframe.x + 0*uw + ep; 
		B[0].R.y = bframe.y + 0*uh + ep;
		B[0].row = 0;
		B[0].column = 0;

		B[1].R.x = bframe.x + 3*uw + ep; 
		B[1].R.y = bframe.y + 0*uh + ep;
		B[1].row = 0;
		B[1].column = 3;

		B[2].R.x = bframe.x + 0*uw + ep; 
		B[2].R.y = bframe.y + 3*uh + ep;
		B[2].row = 3;
		B[2].column = 0;

		B[3].R.x = bframe.x + 3*uw + ep; 
		B[3].R.y = bframe.y + 3*uh + ep;
		B[3].row = 3;
		B[3].column = 3;

		B[4].R.x = bframe.x + 1*uw + ep; 
		B[4].R.y = bframe.y + 2*uh + ep;
		B[4].row = 2;
		B[4].column = 1;
		
		B[5].R.x = bframe.x + 1*uw + ep; 
		B[5].R.y = bframe.y + 3*uh + ep;
		B[5].row = 3;
		B[5].column = 1;

		B[6].R.x = bframe.x + 2*uw + ep; 
		B[6].R.y = bframe.y + 3*uh + ep;
		B[6].row = 3;
		B[6].column = 2;


		B[7].R.x = bframe.x + 1*uw + ep;
		B[7].R.y = bframe.y + 4*uh + ep;
		B[7].row = 4;
		B[7].column = 1;


		B[8].R.x = bframe.x + 2*uw + ep; 
		B[8].R.y = bframe.y + 4*uh + ep;
		B[8].row = 4;
		B[8].column = 2;

		B[9].R.x = bframe.x + 1*uw + ep;
		B[9].R.y = bframe.y + 0*uh + ep;
		B[9].row = 0;
		B[9].column = 1;

}
/* return a block containing (x,y), or NULL if none exists. */
block* findBlock(int x, int y)
{
	/* NOTE: we go backwards to be compatible with z-order */
	for (int i = NBLOCKS-1; i >= 0; i--) {
		if (B[i].R.x <= x && x <= B[i].R.x + B[i].R.w &&
				B[i].R.y <= y && y <= B[i].R.y + B[i].R.h)
			return (B+i);
	}
	return NULL;
}

void close()
{
	SDL_DestroyRenderer(gRenderer); gRenderer = NULL;
	SDL_DestroyWindow(gWindow); gWindow = NULL;
	SDL_Quit();
}

void render()
{
	/* draw entire screen to be black: */
	SDL_SetRenderDrawColor(gRenderer, 0x00, 0x00, 0x00, 0xff);
	SDL_RenderClear(gRenderer);

	/* first, draw the frame: */
	int& W = SCREEN_WIDTH;
	int& H = SCREEN_HEIGHT;
	int w = bframe.w;
	int h = bframe.h;
	SDL_SetRenderDrawColor(gRenderer, 0x39, 0x39, 0x39, 0xff);
	SDL_RenderDrawRect(gRenderer, &bframe);
	/* make a double frame */
	SDL_Rect rframe(bframe);
	int e = 3;
	rframe.x -= e; 
	rframe.y -= e;
	rframe.w += 2*e;
	rframe.h += 2*e;
	SDL_RenderDrawRect(gRenderer, &rframe);

	/* draw some grid lines: */
	SDL_Point p1,p2;
	SDL_SetRenderDrawColor(gRenderer, 0x19, 0x19, 0x1a, 0xff);
	/* vertical */
	p1.x = (W-w)/2;
	p1.y = (H-h)/2;
	p2.x = p1.x;
	p2.y = p1.y + h;
	for (size_t i = 1; i < 4; i++) {
		p1.x += w/4;
		p2.x += w/4;
		SDL_RenderDrawLine(gRenderer,p1.x,p1.y,p2.x,p2.y);
	}
	/* horizontal */
	p1.x = (W-w)/2;
	p1.y = (H-h)/2;
	p2.x = p1.x + w;
	p2.y = p1.y;
	for (size_t i = 1; i < 5; i++) {
		p1.y += h/5;
		p2.y += h/5;
		SDL_RenderDrawLine(gRenderer,p1.x,p1.y,p2.x,p2.y);
	}
	SDL_SetRenderDrawColor(gRenderer, 0xd8, 0xde, 0xe9, 0x7f);
	SDL_Rect goal = {bframe.x + w/4 + ep, bframe.y + 3*h/5 + ep,
	                 w/2 - 2*ep, 2*h/5 - 2*ep};
	SDL_RenderDrawRect(gRenderer,&goal);

	/* now iterate through and draw the blocks */
	drawBlocks();
	/* finally render contents on screen, which should happen once every
	 * vsync for the display */
	SDL_RenderPresent(gRenderer);
}
// Save puzzle
void savePuzzle()
{

}
struct Node 
{
	Node* parent;
	vector<Node> children;
	int board_rep[5][4];

	bool exist(int cb[5][4])
	{
		bool e = true;
		int i; // Column
		int j; // Row
		for (i = 0; i <  board_height; i++) 
		{
			for (j = 0; j < board_width; j++) 
			{
				if(board_rep[i][j] != cb[i][j])
				{
					e = false;
				}
			}
		}
		return e;
	}

	void addBoard(int cb[5][4])
	{
		int i; // Column
		int j; // Row
		for (i = 0; i <  board_height; i++) 
		{
			for (j = 0; j < board_width; j++) 
			{
				board_rep[i][j] = cb[i][j];
			}
		}
	}

	bool solution()
	{
		bool s = false;
		if(board_rep[3][1] == 9 && board_rep[3][2] == 9 && board_rep[4][1] == 9 && board_rep[4][2] == 9)
		{
			s = true;
		}
		return s;
	}
	void addChild(int b[5][4])
	{
		Node child;
		child.addBoard(b);
		children.push_back(child);
		child.parent = this;
		//----//
		int i, j;
		printf("Board representation\n\n");
		for (i = 0; i <  board_height; i++) {
			for (j = 0; j < board_width; j++) {
				// Print board value with spacing of 3
				printf("%3d", b[i][j]);
			}
			printf("\n");
		}
	}
	void Up(int theBlock, int iZero, int jZero)
	{
		int new_board_rep[5][4];
		int i; // Column
		int j; // Row
		for (i = 0; i <  board_height; i++) 
		{
			for (j = 0; j < board_width; j++) 
			{
				new_board_rep[i][j] = board_rep[i][j];
			}
		}

		if(theBlock == 6) // Square
		{
			new_board_rep[iZero][jZero] = 6;
			new_board_rep[iZero][jZero+1] = 0;
			addBoard(new_board_rep);
		}
		else if(theBlock == 3) // Vertical
		{
			new_board_rep[iZero][jZero] = 3;
			new_board_rep[iZero][jZero+1] = 4;
			new_board_rep[iZero][jZero+2] = 0;
			addBoard(new_board_rep);
		}
		else if(theBlock == 1) // Horizontal
		{
			if(board_rep[iZero+1][jZero] == 0)
			{
				new_board_rep[iZero][jZero] = 1;
				new_board_rep[iZero+1][jZero] = 2;
				new_board_rep[iZero][jZero+1] = 0;
				new_board_rep[iZero+1][jZero+1] = 0;
				addBoard(new_board_rep);
			}
		}
		else if(theBlock == 2) // Horizontal
		{
			if(board_rep[iZero-1][jZero] == 0)
			{
				new_board_rep[iZero][jZero] = 2;
				new_board_rep[iZero-1][jZero] = 1;
				new_board_rep[iZero][jZero+1] = 0;
				new_board_rep[iZero-1][jZero+1] = 0;
				addBoard(new_board_rep);
			}
		}
		else if(theBlock == 9) // Large Square
		{
			if(board_rep[iZero+1][jZero] == 0)
			{
				new_board_rep[iZero][jZero] = 5;
				new_board_rep[iZero+1][jZero] = 5;
				new_board_rep[iZero][jZero+2] = 0;
				new_board_rep[iZero+1][jZero+2] = 0;
				addBoard(new_board_rep);
			}
			else if(board_rep[iZero-1][jZero] == 0)
			{
				new_board_rep[iZero][jZero] = 5;
				new_board_rep[iZero-1][jZero] = 5;
				new_board_rep[iZero][jZero+2] = 0;
				new_board_rep[iZero-1][jZero+2] = 0;
				addBoard(new_board_rep);
			}
		}
	}

	void Down(int theBlock, int iZero, int jZero)
	{
		int new_board_rep[5][4];
		int i; // Column
		int j; // Row
		for (i = 0; i <  board_height; i++) 
		{
			for (j = 0; j < board_width; j++) 
			{
				new_board_rep[i][j] = board_rep[i][j];
			}
		}
		if(theBlock == 6) // Square
		{
			new_board_rep[iZero][jZero] = 6;
			new_board_rep[iZero][jZero-1] = 0;
			addBoard(new_board_rep);
		}
		else if(theBlock == 4) // Vertical
		{
			new_board_rep[iZero][jZero] = 4;
			new_board_rep[iZero][jZero-1] = 3;
			new_board_rep[iZero][jZero-2] = 0;
			addBoard(new_board_rep);
		}
		else if(theBlock == 1) // Vertical
		{
			if(board_rep[iZero+1][jZero] == 0)
			{
				new_board_rep[iZero][jZero] = 1;
				new_board_rep[iZero+1][jZero] = 2;
				new_board_rep[iZero][jZero-1] = 0;
				new_board_rep[iZero+1][jZero-1] = 0;
				addBoard(new_board_rep);
			}
		}
		else if(theBlock == 2) // Horizontal
		{
			if(board_rep[iZero-1][jZero] == 0)
			{
				new_board_rep[iZero][jZero] = 2;
				new_board_rep[iZero-1][jZero] = 1;
				new_board_rep[iZero][jZero-1] = 0;
				new_board_rep[iZero-1][jZero-1] = 0;
				addBoard(new_board_rep);
			}
		}
		else if(theBlock == 9) // Large Square
		{
			if(board_rep[iZero+1][jZero] == 0)
			{
				new_board_rep[iZero][jZero] = 5;
				new_board_rep[iZero+1][jZero] = 5;
				new_board_rep[iZero][jZero-2] = 0;
				new_board_rep[iZero+1][jZero-2] = 0;
				addBoard(new_board_rep);
			}
			else if(board_rep[iZero-1][jZero] == 0)
			{
				new_board_rep[iZero][jZero] = 5;
				new_board_rep[iZero-1][jZero] = 5;
				new_board_rep[iZero][jZero-2] = 0;
				new_board_rep[iZero-1][jZero-2] = 0;
				addBoard(new_board_rep);
			}
		}
	}

	void Right(int theBlock, int iZero, int jZero)
	{
		int new_board_rep[5][4];
		int i; // Column
		int j; // Row
		for (i = 0; i <  board_height; i++) 
		{
			for (j = 0; j < board_width; j++) 
			{
				new_board_rep[i][j] = board_rep[i][j];
			}
		}
		if(theBlock == 6) // Square
		{
			new_board_rep[iZero][jZero] = 6;
			new_board_rep[iZero-1][jZero] = 0;
			addBoard(new_board_rep);
		}
		else if(theBlock == 3) // Vertical
		{
			if(board_rep[iZero][jZero+1] == 0)
			{
				new_board_rep[iZero][jZero] = 3;
				new_board_rep[iZero][jZero+1] = 4;
				new_board_rep[iZero-1][jZero] = 0;
				new_board_rep[iZero-1][jZero+1] = 0;
				addBoard(new_board_rep);
			}
		}
		else if(theBlock == 4) // Vertical
		{
			if(board_rep[iZero][jZero-1] == 0)
			{
				new_board_rep[iZero][jZero] = 4;
				new_board_rep[iZero][jZero+1] = 3;
				new_board_rep[iZero-1][jZero] = 0;
				new_board_rep[iZero-1][jZero-1] = 0;
				addBoard(new_board_rep);
			}
		}
		else if(theBlock == 2) // Horizontal
		{
			new_board_rep[iZero][jZero] = 2;
			new_board_rep[iZero-1][jZero] = 1;
			new_board_rep[iZero-2][jZero] = 0;
			addBoard(new_board_rep);
		}
		else if(theBlock == 9) // Large Square
		{
			if(board_rep[iZero][jZero+1] == 0)
			{
				new_board_rep[iZero][jZero] = 5;
				new_board_rep[iZero][jZero+1] = 5;
				new_board_rep[iZero-2][jZero] = 0;
				new_board_rep[iZero-2][jZero+1] = 0;
				addBoard(new_board_rep);
			}
			else if(board_rep[iZero][jZero-1] == 0)
			{
				new_board_rep[iZero][jZero] = 5;
				new_board_rep[iZero][jZero-1] = 5;
				new_board_rep[iZero-2][jZero] = 0;
				new_board_rep[iZero-2][jZero-1] = 0;
				addBoard(new_board_rep);
			}
		}
	}

	void Left(int theBlock, int iZero, int jZero)
	{
		int new_board_rep[5][4];
		int i; // Column
		int j; // Row
		for (i = 0; i <  board_height; i++) 
		{
			for (j = 0; j < board_width; j++) 
			{
				new_board_rep[i][j] = board_rep[i][j];
			}
		}
		if(theBlock == 6) // Square
		{
			new_board_rep[iZero][jZero] = 6;
			new_board_rep[iZero+1][jZero] = 0;
			addBoard(new_board_rep);
		}
		else if(theBlock == 3) // Vertical
		{
			if(board_rep[iZero][jZero+1] == 0)
			{
				new_board_rep[iZero][jZero] = 3;
				new_board_rep[iZero][jZero+1] = 4;
				new_board_rep[iZero+1][jZero] = 0;
				new_board_rep[iZero+1][jZero+1] = 0;
				addBoard(new_board_rep);
			}
		}
		else if(theBlock == 4) // Vertical
		{
			if(board_rep[iZero][jZero-1] == 0)
			{
				new_board_rep[iZero][jZero] = 4;
				new_board_rep[iZero][jZero+1] = 3;
				new_board_rep[iZero+1][jZero] = 0;
				new_board_rep[iZero+1][jZero+1] = 0;
				addBoard(new_board_rep);
			}
		}
		else if(theBlock == 1) // Horizontal
		{
			new_board_rep[iZero][jZero] = 1;
			new_board_rep[iZero+1][jZero] = 2;
			new_board_rep[iZero+2][jZero] = 0;
			addBoard(new_board_rep);
		}
		else if(theBlock == 9) // Large Square
		{
			if(board_rep[iZero][jZero+1] == 0)
			{
				new_board_rep[iZero][jZero] = 5;
				new_board_rep[iZero][jZero+1] = 5;
				new_board_rep[iZero+2][jZero] = 0;
				new_board_rep[iZero+2][jZero+1] = 0;
				addBoard(new_board_rep);
			}
			else if(board_rep[iZero][jZero-1] == 0)
			{
				new_board_rep[iZero][jZero] = 5;
				new_board_rep[iZero][jZero-1] = 5;
				new_board_rep[iZero+2][jZero] = 0;
				new_board_rep[iZero+2][jZero-1] = 0;
				addBoard(new_board_rep);
			}
		}
	}

	void spandMove()
	{
		int theBlock = 0;
		deque<int> zero;
		int i; // Column
		int j; // Row
		for (i = 0; i <  board_height; i++) 
		{
			for (j = 0; j < board_width; j++) 
			{
				// Print board value with spacing of 3
				if(board_rep[i][j] == 0)
				{
					if(j-1 >= 0)
					{
						theBlock = board_rep[i][j-1];
						Up(theBlock, i, j);
					}

					if(j+1 < 5)
					{
						theBlock = board_rep[i][j+1];
						Down(theBlock, i, j);
					}

					if(i-1 >= 0)
					{
						theBlock = board_rep[i-1][j];
						Left(theBlock, i, j);
					}

					if(i+1 < 4)
					{
						theBlock = board_rep[i+1][j];
						Right(theBlock, i, j);
					}
				}
			}
		}
		
	}

};

void snap(block* b)
{
	/* TODO: once you have established a representation for configurations,
	 * you should update this function to make sure the configuration is
	 * updated when blocks are placed on the board, or taken off.  */
	assert(b != NULL);
	/* upper left of grid element (i,j) will be at
	 * bframe.{x,y} + (j*bframe.w/4,i*bframe.h/5) */
	/* translate the corner of the bounding box of the board to (0,0). */
	int x = b->R.x - bframe.x;
	int y = b->R.y - bframe.y;
	int uw = bframe.w/4;
	int uh = bframe.h/5;
	/* NOTE: in a perfect world, the above would be equal. */
	int i = (y+uh/2)/uh; /* row */
	int j = (x+uw/2)/uw; /* col */
	if (0 <= i && i < 5 && 0 <= j && j < 4) {
		b->R.x = bframe.x + j*uw + ep;
		b->R.y = bframe.y + i*uh + ep;
	}
}

bool itExist(deque<Node> d, Node c)
{
	bool exist = false;

	for (size_t i = 0; i < d.size(); i++)
	{
		if(d[i].exist(c.board_rep))
		{
			exist = true;
		}
	}
	return exist;
}

void traceSolution(deque<Node> solution, Node matrix)
{
	Node current = matrix;
	solution.push_back(current);

	while(current.parent != NULL)
	{
		current = *current.parent;
		solution.push_back(current);
	}
}

deque<Node> BreadthFirstSearch(Node root)
{
	deque<Node> solutionPath;
    deque<Node> closeList;
	deque<Node> openList;

    bool solutionFound = false;
	openList.push_back(root);
	while(solutionFound == false && openList.size() > 0)
	{
		Node currentNode = openList[0];
		closeList.push_back(currentNode);
		openList.pop_front();
		currentNode.spandMove();

		for (size_t i = 0; i < currentNode.children.size(); i++)
		{
			Node currentChild = currentNode.children[i];
			if(currentChild.solution())
			{
				solutionFound = true;
				traceSolution(solutionPath, currentChild);
			}
			if((!itExist(openList, currentChild)) && !itExist(closeList, currentChild))
			{
				openList.push_back(currentChild);
			}
		}

	}
	return solutionPath;
}

int main(int argc, char *argv[])
{
	/* TODO: add option to specify starting state from cmd line? */
	/* start SDL; create window and such: */
	if(!init()) {
		printf( "Failed to initialize from main().\n" );
		return 1;
	}
	atexit(close);
	bool quit = false; /* set this to exit main loop. */
	SDL_Event e;

	setPuzzle();
	Node firstNode;
	int new_board_rep[5][4];
	int i; // Column
	int j; // Row
	for (i = 0; i <  board_height; i++) 
	{
		for (j = 0; j < board_width; j++) 
		{
			new_board_rep[i][j] = board_r[i][j];
		}
	}
	firstNode.addBoard(new_board_rep);
	deque<Node> path;
	path = BreadthFirstSearch(firstNode);

	/* main loop: */
	while(!quit) {
		/* handle events */
		while(SDL_PollEvent(&e) != 0) {
			/* meta-q in i3, for example: */
			if(e.type == SDL_MOUSEMOTION) {
				if (mousestate == 1 && dragged) {
					int dx = e.button.x - lastm.x;
					int dy = e.button.y - lastm.y;
					lastm.x = e.button.x;
					lastm.y = e.button.y;
					dragged->R.x += dx;
					dragged->R.y += dy;
				}
			} else if (e.type == SDL_MOUSEBUTTONDOWN) {
				if (e.button.button == SDL_BUTTON_RIGHT) {
					block* b = findBlock(e.button.x,e.button.y);
					if (b) b->rotate();
				} else {
					mousestate = 1;
					lastm.x = e.button.x;
					lastm.y = e.button.y;
					dragged = findBlock(e.button.x,e.button.y);
				}
				/* XXX happens if during a drag, someone presses yet
				 * another mouse button??  Probably we should ignore it. */
			} else if (e.type == SDL_MOUSEBUTTONUP) {
				if (e.button.button == SDL_BUTTON_LEFT) {
					mousestate = 0;
					lastm.x = e.button.x;
					lastm.y = e.button.y;
					if (dragged) {
						/* snap to grid if nearest location is empty. */
						snap(dragged);
					}
					dragged = NULL;
				}
			} else if (e.type == SDL_QUIT) {
				quit = true;
			} else if (e.type == SDL_KEYDOWN) {
				switch (e.key.keysym.sym) {
					case SDLK_ESCAPE:
					case SDLK_q:
						quit = true;
						break;
					case SDLK_LEFT:
						/* TODO: show previous step of solution */
						break;
					case SDLK_RIGHT:
						/* TODO: show next step of solution */
						break;
					case SDLK_p:
						/* TODO: print the state to stdout
						 * (maybe for debugging purposes...) */
						computeBoardRepresentation();
						printBoardRepresentation();
						break;
					case SDLK_s:
						/* TODO: try to find a solution */
						break;
					default:
						break;
				}
			}
		}
		fcount++;
		render();
	}

	printf("total frames rendered: %i\n",fcount);
	return 0;
}
